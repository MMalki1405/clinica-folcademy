package com.folcademy.clinica.exceptions;

public class BadRequestException extends RuntimeException {
        public BadRequestException(String message) {
            super(message);
    }
}
